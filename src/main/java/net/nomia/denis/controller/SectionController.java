package net.nomia.denis.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.nomia.denis.dto.SectionDto;
import net.nomia.denis.service.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(SectionController.PATH)
public class SectionController {

    public static final String PATH = "/api/section";

    private final SectionService service;

    @Autowired
    public SectionController(SectionService service) {
        this.service = service;
    }

    @ApiOperation(value = "Get all sections list")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list")
    })
    @GetMapping
    public List<SectionDto> getAll(){
        return service.getAll();
    }

    @ApiOperation(value = "Get all ROOT sections list")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list")
    })
    @GetMapping("/roots")
    public List<SectionDto> getAllRoots(){
        return service.getAllRoots();
    }

    @ApiOperation(value = "Get the section")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved section")
    })
    @GetMapping(value = "/{id}")
    public SectionDto getOne(@PathVariable long id) {
        return service.get(id);
    }

    @ApiOperation(value = "Create the section")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully created section")
    })
    @PostMapping
    public SectionDto create(@RequestBody SectionDto sectionDto){
        return service.create(sectionDto);
    }

    @ApiOperation(value = "Update the section")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated section")
    })
    @PatchMapping
    public SectionDto update(@RequestBody SectionDto sectionDto) {
        return service.update(sectionDto);
    }

    @ApiOperation(value = "Delete the section")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted section")
    })
    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable long id) {
        service.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
