package net.nomia.denis.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import net.nomia.denis.dto.ProductDto;
import net.nomia.denis.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ProductController.PATH)
public class ProductController {

    public static final String PATH = "/api/product";

    private final ProductService service;

    public ProductController(ProductService service) {
        this.service = service;
    }

    @ApiOperation(value = "View a list of available products")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list")
    })
    @GetMapping
    public List<ProductDto> getAll(){
        return service.getAll();
    }

    @ApiOperation(value = "Get the product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved product")
    })
    @GetMapping(value = "/{id}")
    public ProductDto getOne(@PathVariable long id) {
        return service.get(id);
    }

    @ApiOperation(value = "Create product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully created product")
    })
    @PostMapping
    public ProductDto create(@RequestBody ProductDto productDto){
        return service.create(productDto);
    }

    @ApiOperation(value = "Update the product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated product")
    })
    @PatchMapping
    public ProductDto update(@RequestBody ProductDto productDto) {
        return service.update(productDto);
    }

    @ApiOperation(value = "Delete the product")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully deleted product")
    })
    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable long id) {
        service.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
