package net.nomia.denis.dto;

/**
 * Интерфейс для дто объектов с id
 */
public interface ObjectWithIdDto {

    /**
     * Получение id.
     * 
     * @return id
     */
    Long getId();

    /**
     * Установка id
     * 
     * @param id
     *            id
     */
    void setId(Long id);
}
