package net.nomia.denis.repository;

import net.nomia.denis.model.Section;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SectionRepository extends JpaRepository<Section, Long> {

    List<Section> findAllByParentIsNullOrderByNameAsc();

}
