package net.nomia.denis;

/**
 * Профили запуска приложения
 */
public final class StarterProfiles {

    /**
     * default
     */
    public static final String DEFAULT = "default";

    /**
     * test
     */
    public static final String TEST = "test";

    /**
     * staging
     */
    public static final String STAGING = "staging";

    /**
     * production
     */
    public static final String PRODUCTION = "production";

    /**
     * production
     */
    public static final String DEV = "dev";

    private StarterProfiles() {
        // Non-instantiable class
    }

}
