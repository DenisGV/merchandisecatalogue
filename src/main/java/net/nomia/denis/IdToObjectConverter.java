package net.nomia.denis;

import ma.glasnost.orika.MappingContext;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.metadata.Type;
import net.nomia.denis.model.ObjectWithId;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.data.repository.support.DomainClassConverter;

import java.util.Optional;

/**
 * IdToObjectConverter
 *
 * @param <T> ObjectWithId
 */
public class IdToObjectConverter<T extends ObjectWithId> extends BidirectionalConverter<Long, T> {

    private DomainClassConverter domainClassConverter;

    /**
     * IdToObjectConverter
     *
     * @param domainClassConverter domainClassConverter
     */
    public IdToObjectConverter(DomainClassConverter domainClassConverter) {
        super();
        this.domainClassConverter = domainClassConverter;
    }

    @Override
    public boolean canConvert(Type<?> sourceType, Type<?> destinationType) {
        return (this.sourceType.isAssignableFrom(sourceType) && this.destinationType.isAssignableFrom(destinationType)) || (
                this.destinationType.isAssignableFrom(sourceType) && this.sourceType.isAssignableFrom(destinationType));
    }

    @Override
    public T convertTo(Long source, Type<T> destinationType, MappingContext mappingContext) {
        Object result = domainClassConverter
                .convert(source, TypeDescriptor.valueOf(Long.class), TypeDescriptor.valueOf(destinationType.getRawType()));
        if (result instanceof Optional) {
            return ((Optional<T>) result).get();
        }
        if (result instanceof Long) {
            //Объект не был найден в БД
            LoggerFactory.getLogger(this.getClass())
                    .warn("Couldn't convert " + destinationType.getCanonicalName() + " with id " + source + " to entity.");
            return null;
        }
        return (T) result;
    }

    @Override
    public Long convertFrom(T source, Type<Long> destinationType, MappingContext mappingContext) {
        return source.getId();
    }

}
