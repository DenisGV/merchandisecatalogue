package net.nomia.denis.service;

import net.nomia.denis.OrikaBeanMapper;
import net.nomia.denis.dto.SectionDto;
import net.nomia.denis.model.Section;
import net.nomia.denis.repository.SectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SectionService {

    private final SectionRepository repository;

    private final OrikaBeanMapper mapper;

    @Autowired
    public SectionService(SectionRepository repository, OrikaBeanMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Transactional(readOnly = true)
    public List<SectionDto> getAll(){
        return mapper.mapAsList(repository.findAll(), SectionDto.class);
    }

    @Transactional(readOnly = true)
    public List<SectionDto> getAllRoots(){
        return mapper.mapAsList(repository.findAllByParentIsNullOrderByNameAsc(), SectionDto.class);
    }

    @Transactional(readOnly = true)
    public SectionDto get(long id) {
        return mapper.map(repository.getOne(id), SectionDto.class);
    }

    public SectionDto create(SectionDto sectionDto){
        Section section = mapper.map(sectionDto, Section.class);
        return mapper.map(repository.save(section), SectionDto.class);
    }

    public SectionDto update(SectionDto sectionDto){
        Section section = repository.getOne(sectionDto.getId());
        mapper.map(sectionDto, section);
        return mapper.map(section, SectionDto.class);
    }

    public void delete(long id) {
        repository.deleteById(id);
    }

}
