package net.nomia.denis.service;

import net.nomia.denis.OrikaBeanMapper;
import net.nomia.denis.dto.ProductDto;
import net.nomia.denis.model.Product;
import net.nomia.denis.repository.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProductService {

    private final ProductRepository repository;

    private final OrikaBeanMapper mapper;

    public ProductService(ProductRepository repository, OrikaBeanMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Transactional(readOnly = true)
    public List<ProductDto> getAll() {
        return mapper.mapAsList(repository.findAll(), ProductDto.class);
    }

    @Transactional(readOnly = true)
    public ProductDto get(long id) {
        return mapper.map(repository.getOne(id), ProductDto.class);
    }

    public ProductDto create(ProductDto productDto) {
        Product product = mapper.map(productDto, Product.class);
        return mapper.map(repository.save(product), ProductDto.class);
    }

    public ProductDto update(ProductDto productDto) {
        Product product = repository.getOne(productDto.getId());
        mapper.map(productDto, product);
        return mapper.map(product, ProductDto.class);
    }

    public void delete(long id) {
        repository.deleteById(id);
    }
}
