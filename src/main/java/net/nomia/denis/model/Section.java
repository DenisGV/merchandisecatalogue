package net.nomia.denis.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Section implements ObjectWithId{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Section parent;

    @Column(nullable = false, unique = true)
    private String name;

}
