package net.nomia.denis.model;

/**
 * Интерфейс для объектов с id
 */
public interface ObjectWithId {

    /**
     * Получение id.
     * 
     * @return id
     */
    Long getId();

    /**
     * Установка id
     * 
     * @param id
     *            id
     */
    void setId(Long id);
}
