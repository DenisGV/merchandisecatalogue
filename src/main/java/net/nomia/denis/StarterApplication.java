package net.nomia.denis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Arrays;

/**
 * Запускатель приложения
 */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class, ManagementWebSecurityAutoConfiguration.class})
@EnableAsync
@EnableScheduling
@EnableAspectJAutoProxy
@EnableTransactionManagement(proxyTargetClass = true)
public class StarterApplication extends SpringApplication {

    /**
     * Конструктор
     */
    public StarterApplication() {
        super(net.nomia.denis.StarterApplication.class);
    }


    /**
     * Запуск приложения
     *
     * @param args
     *            список аргументов запуска
     */
    public static void main(final String... args) {
        new net.nomia.denis.StarterApplication().run(args);
    }

    @Override
    protected void configureProfiles(final ConfigurableEnvironment environment, final String[] args) {
        super.configureProfiles(environment, args);

        final boolean defaultProfile = environment.acceptsProfiles(StarterProfiles.DEFAULT);
        final boolean stagingActive = environment.acceptsProfiles(StarterProfiles.STAGING);
        final boolean productionActive = environment.acceptsProfiles(StarterProfiles.PRODUCTION);
        final boolean devActive = environment.acceptsProfiles(StarterProfiles.DEV);


        if (defaultProfile) {
            System.out.println("Activating default profile");
        } else if (stagingActive && productionActive) {
            throw new IllegalStateException("Cannot activate staging and production profiles at the same time.");
        } else if (productionActive || stagingActive || devActive) {
            System.out.println("Activating "
                    + Arrays.toString(environment.getActiveProfiles())
                    + " profile.");
        } else {
            throw new IllegalStateException("Unknown profiles specified.");
        }

        Arrays.stream(environment.getActiveProfiles()).filter(p -> p.startsWith("module-"))
                .forEach(p -> System.out.println("Activate module: " + p.substring("module-".length())));
    }
}
