package net.nomia.denis;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;

/**
 * Обработчик исключений контроллеров
 */
@ControllerAdvice
public class ExceptionHandlingController extends ResponseEntityExceptionHandler {

    /**
     * Обработка исключения нарушения ограничений
     * 
     * @param e
     *            исключение
     * @return ответ с сообщением
     */
    @ResponseBody
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity handleConstraintViolation(ConstraintViolationException e) {
        return formConstraintViolationMessage(e);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        logger.warn("Returning HTTP 400 Bad Request", ex);
        return super.handleHttpMessageNotReadable(ex, headers, status, request);
    }


    @ResponseBody
    @ExceptionHandler(TransactionSystemException.class)
    public ResponseEntity handleNestedConstraintViolation(TransactionSystemException e) {
        if (e.getMostSpecificCause().getClass().equals(ConstraintViolationException.class)) {
            return formConstraintViolationMessage(((ConstraintViolationException) e.getMostSpecificCause()));
        }
        logger.warn("TransactionSystemException", e);
        return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity formConstraintViolationMessage(ConstraintViolationException e) {
        ObjectNode violated = JsonNodeFactory.instance.objectNode();
        e.getConstraintViolations().forEach(violation -> {
            StringBuilder prop = new StringBuilder();
            violation.getPropertyPath().forEach(node -> prop.append(node.getName()));
            violated.put(prop.toString(), violation.getMessage());
        });
        logger.warn("ConstraintViolationException", e);
        return new ResponseEntity<>(violated.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
